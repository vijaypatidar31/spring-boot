package com.example.course.repositories;

import com.example.course.models.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course,String> {

    Iterable<Course> findAllByTopicId(String topicId);

}
