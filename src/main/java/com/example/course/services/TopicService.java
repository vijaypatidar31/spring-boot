package com.example.course.services;

import com.example.course.models.Topic;
import com.example.course.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TopicService {

    @Autowired
    private TopicRepository repository;

    public List<Topic> getTopics() {
        List<Topic> topics = new ArrayList<>();
        repository.findAll()
                .forEach(topics::add);
        return topics;
    }

    public Topic getTopicById(String id) {
        Optional<Topic> optionalTopic = repository.findById(id);
        return optionalTopic.orElse(null);
    }

    public void addTopic(Topic topic) {
        repository.save(topic);
    }


    public void deleteTopicById(String topicId) {
        repository.deleteById(topicId);
    }

    public void updateTopic(Topic topic) {
        repository.save(topic);
    }
}
