package com.example.course.services;

import com.example.course.models.Course;
import com.example.course.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository repository;

    public List<Course> getAllCourses(String topicId) {
        List<Course> courses = new ArrayList<>();
        repository.findAllByTopicId(topicId).forEach(courses::add);
        return courses;
    }

    public Course getCourseById(String courseId) {
        return repository.findById(courseId).orElse(null);
    }

    public void addCourse(Course course) {
        repository.save(course);
    }

    public void deleteById(String courseId) {
        repository.deleteById(courseId);
    }

    public void updateCourse(Course course) {
        repository.save(course);
    }


}
