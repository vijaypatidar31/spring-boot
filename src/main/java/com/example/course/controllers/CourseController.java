package com.example.course.controllers;

import com.example.course.models.Course;
import com.example.course.models.Topic;
import com.example.course.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping("/topics/{topicId}/courses")
    public List<Course> getCourses(@PathVariable String topicId) {
        return courseService.getAllCourses(topicId);
    }

    @GetMapping("/topics/{topicId}/courses/{courseId}")
    public Course getTopicById(@PathVariable String topicId, @PathVariable String courseId) {
        return courseService.getCourseById(courseId);
    }

    @PostMapping("/topics/{topicId}/courses")
    public void addCourse(@RequestBody Course course, @PathVariable String topicId) {
        course.setTopic(new Topic(topicId));
        System.out.println(course);
        courseService.addCourse(course);
    }

    @DeleteMapping("/topics/{topicId}/courses/{courseId}")
    public void deleteCourse(@PathVariable String topicId, @PathVariable String courseId) {
        courseService.deleteById(courseId);
    }

    @PutMapping("/topics/{topicId}/courses")
    public void updateCourse(@RequestBody Course course) {
        courseService.updateCourse(course);
    }

}
