package com.example.course.controllers;

import com.example.course.models.Topic;
import com.example.course.services.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TopicController {

    @Autowired
    private TopicService topicService;

    @GetMapping("/topics")
    public List<Topic> getTopics() {
        return topicService.getTopics();
    }

    @GetMapping("/topics/{topicId}")
    public Topic getTopicById(@PathVariable String topicId) {
        return topicService.getTopicById(topicId);
    }

    @PostMapping(value = "/topics")
    public void addTopic(@RequestBody Topic topic) {
        topicService.addTopic(topic);
    }

    @DeleteMapping("/topics/{topicId}")
    public void deleteTopic(@PathVariable String topicId) {
        topicService.deleteTopicById(topicId);
    }

    @PutMapping("/topics")
    public void updateTopic(@RequestBody Topic topic) {
        topicService.updateTopic(topic);
    }

}
